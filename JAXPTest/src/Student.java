
public class Student {

	private String id;
	private String first_name;
	private String last_name;
	private String email;
	private String phone;
	
	public Student(String id) {
		this.id = id;
	}

	public void setFirstName(String str) {
		first_name = str;
	}
	
	public void setLastName(String str) {
		last_name = str;
	}

	public void setEmail(String str) {
		email = str;
	}

	public void setPhone(String str) {
		phone = str;
	}

	public String Id() {
		return id;
	}

	public String FirstName() {
		return first_name;
	}
	
	public String LastName() {
		return last_name;
	}

	public String Email() {
		return email;
	}

	public String Phone() {
		return phone;
	}
}
