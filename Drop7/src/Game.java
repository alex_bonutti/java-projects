import java.util.Random;

import javax.swing.JOptionPane;


public class Game {

	final private int EMPTY_CELL = 0;
	final private int POINTS = 7;
	final private int NEXT_DROP = 5;

	private Pawn[] pawns;
	private int[][] playboard;
	private int[] stacks;
	private int nextPawn;
	private int level;
	private int points;
	private int next_drop;

	private boolean[][] toRemove;
	private int pawns_removed;
	private int[][] toPop;
	
	public Game() {

		int i, j;
		points = 0;
		next_drop = NEXT_DROP;

		pawns = new Pawn[BoardSettings.PAWNS_DIMENSION];
		for (i = 1; i <= BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			pawns[i] = new Pawn(Integer.toString(i));
		}
		pawns[BoardSettings.BROKEN_PAWN_ID] = new Pawn(Integer.toString(BoardSettings.BROKEN_PAWN_ID));		
		pawns[BoardSettings.FULL_PAWN_ID] = new Pawn(Integer.toString(BoardSettings.FULL_PAWN_ID));
		
		playboard = new int[BoardSettings.BOARD_CELLS_DIMENSION][BoardSettings.BOARD_CELLS_DIMENSION];
		stacks = new int[BoardSettings.BOARD_CELLS_DIMENSION];
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			stacks[i] = 0;
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				// board initialization as empty
				playboard[i][j] = EMPTY_CELL;
			}
		}
		
		toRemove = new boolean[BoardSettings.BOARD_CELLS_DIMENSION][BoardSettings.BOARD_CELLS_DIMENSION];
		pawns_removed = 0;
		toPop = new int[BoardSettings.BOARD_CELLS_DIMENSION][BoardSettings.BOARD_CELLS_DIMENSION];
	}

	public int Playboard(int x, int y)
	{
		return playboard[x][y];
	}

	public boolean toBeRemoved(int x, int y)
	{
		return toRemove[x][y];
	}

	public Pawn getPawn(int p)
	{
		return pawns[p];
	}

	public int getPoints()
	{
		return points;
	}

	public boolean makeMove (int j, int pl)
    {
		try
		{
	    	if (stacks[j] < BoardSettings.BOARD_CELLS_DIMENSION)
	    	{
	    		level = stacks[j];
		    	playboard[level][j] = pl;
		    	stacks[j]++;
				return true;
	    	}
	    	else
	    		JOptionPane.showMessageDialog(null, Strings.MSG_FULL_STACK);
		}
		catch (Exception e)
		{
    		JOptionPane.showMessageDialog(null, Integer.toString(j)+" "+Integer.toString(pl)+" "+Integer.toString(level));
		}
		
    	return false;
    }

	public int getLevel()
	{
		return level;
	}
	
	public void checkGrid(int y)
    {
		do
		{
			pawns_removed = 0;
			checkColumn(y);
			checkRows();
			removePawns();
			popBlocks();
			checkRows();
		}
		while (pawns_removed > 0);
    }

	public void checkRows()
    {
		int row, i, rowed, from_column = 0, to_column = 0;
		for (row = 0; row < BoardSettings.BOARD_CELLS_DIMENSION; row++)
		{
			rowed = 0;
			for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
			{
				if (rowed == 0)
					from_column = i;
				
				if (playboard[row][i] != 0)
				{
					rowed++;
					to_column = i;
				}
				else if (rowed > 0)
				{
					checkSubRow(row, from_column, to_column, rowed);
					rowed = 0;
				}
			}
			
			if (rowed > 0)
				checkSubRow(row, from_column, to_column, rowed);
		}
    }

	public void checkSubRow(int row, int from_column, int to_column, int rowed)
	{
		int y;
		for (y = from_column; y <= to_column; y++)
		{
			if (playboard[row][y] == rowed)
			{
				toRemove[row][y] = true;
				pawns_removed++;
			}
		}
	}
	
	public void checkColumn(int y)
    {
		int x1;
		for (x1 = 0; x1 < BoardSettings.BOARD_CELLS_DIMENSION; x1++)
		{
			if (stacks[y] > 0 && stacks[y] == playboard[x1][y])
			{
				toRemove[x1][y] = true;
				pawns_removed++;
			}
		}
    }
	
	public void removePawns()
	{
		int x, y;

		/*String msg = new String();
		for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
		{
			msg += "["+ Integer.toString(stacks[y]) +"]";
		}
		JOptionPane.showMessageDialog(null, msg);*/

		for (x = 0; x < BoardSettings.BOARD_CELLS_DIMENSION; x++)
		{
			for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
			{
				if (toRemove[x][y])
				{
					playboard[x][y] = 0;
					stacks[y]--;
	
					points += POINTS;
					toRemove[x][y] = false;
					
					popNeighbor(x, y);
				}
			}
		}

		for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
		{
			fillColumn(y);
		}
    }

	public void fillColumn(int y)
	{
		int x;
		for (x = 0; x < BoardSettings.BOARD_CELLS_DIMENSION; x++)
		{
			if (playboard[x][y] == 0)
			{
				playboard[x][y] = getFirstDroppingPawn(x+1, y);
			}
		}		
	}

	public int getFirstDroppingPawn(int x1, int y)
	{
		int x2, pawn;
		for (x2 = x1; x2 < BoardSettings.BOARD_CELLS_DIMENSION; x2++)
		{
			if (playboard[x2][y] != 0)
			{
				pawn = playboard[x2][y];
				playboard[x2][y] = 0;
				return pawn;
			}
		}
		
		return 0;
	}

	public void nextMove()
	{
		Random rn = new Random();
		nextPawn = rn.nextInt(BoardSettings.BOARD_CELLS_DIMENSION) + 1;
	}

	public int nextPawn()
	{
		return nextPawn;
	}

	public void startGame()
    {
		nextMove();
    }

	/***********************
	 * Drop functions
	 */
	public int getNextDrop()
	{
		return next_drop;
	}

	public void updateNextDrop()
	{
		//if (next_drop > 0)
		next_drop--;
		//else
		//	next_drop = NEXT_DROP;
	}

	public void checkDrop()
	{
		if (next_drop == 0)
		{
			int y;
			for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
			{
				stacks[y]++;
		    	if (stacks[y] <= BoardSettings.BOARD_CELLS_DIMENSION)
		    	{
		    		shiftColumn(y);
		    	}
		    	else
		    	{
		    		JOptionPane.showMessageDialog(null, Strings.MSG_FULL_STACK);
		    		System.exit(1);
		    	}
			}
			for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
				checkGrid(y);
			
			next_drop = NEXT_DROP;			
		}
    }

	public void shiftColumn(int y)
	{
		int x;
		for (x = BoardSettings.BOARD_CELLS_DIMENSION-1; x >= 0; x--)
		{
			//JOptionPane.showMessageDialog(null, "["+Integer.toString(x)+"]["+Integer.toString(y)+"] "+Integer.toString(playboard[x][y]));
			if (playboard[x][y] != 0)
				playboard[x+1][y] = playboard[x][y];
		}
		playboard[0][y] = BoardSettings.FULL_PAWN_ID;
	}

	public void popNeighbor(int x, int y)
	{
		if (x-1 >= 0)
			toPop[x-1][y]++;
		if (y-1 >= 0)
			toPop[x][y-1]++;
		if (x+1 < BoardSettings.BOARD_CELLS_DIMENSION)
			toPop[x+1][y]++;
		if (y+1 < BoardSettings.BOARD_CELLS_DIMENSION)
			toPop[x][y+1]++;
	}

	public void popBlocks()
	{
		int x, y;

		for (x = 0; x < BoardSettings.BOARD_CELLS_DIMENSION; x++)
		{
			for (y = 0; y < BoardSettings.BOARD_CELLS_DIMENSION; y++)
			{
				if (toPop[x][y] > 0)
				{					
					playboard[x][y] = getPopPawn(x, y);
	
					toPop[x][y] = 0;
				}
			}
		}
    }
	
	public int getPopPawn(int x, int y)
	{
		/*String msg = new String();
		msg += "["+ Integer.toString(x) +"]["+ Integer.toString(y) +"] "+ Integer.toString(playboard[x][y]) +"";
		JOptionPane.showMessageDialog(null, msg);*/

		if (playboard[x][y] >= BoardSettings.BROKEN_PAWN_ID)
		{
			int i = playboard[x][y] - toPop[x][y];
			if (i <= BoardSettings.BOARD_CELLS_DIMENSION)
			{
				Random r = new Random();
				return r.nextInt(BoardSettings.BOARD_CELLS_DIMENSION) + 1;
			}
			else
				return BoardSettings.BROKEN_PAWN_ID;
		}
		
		return playboard[x][y];
	}
}
