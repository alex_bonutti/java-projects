package it.apogeo.android.cap05.arrayadaptertest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ArrayAdapterTestActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Impostiamo il Layout
		setContentView(R.layout.main);
		
		// Otteniamo il riferimento alla ListView
		ListView listView = (ListView) findViewById(R.id.arrayList);
		
		// Otteniamo il riferimento all'array dei dati
		final String[] arrayData = getResources().getStringArray(R.array.array_data); 

		// Creiamo l'ArrayAdapter => crea tanti elementi R.layout.row quante sono le stringhe in arrayData
		// NB: ArrayAdapter � una collezione di oggetti, che possono essere di diverso tipo.
		// <String> � un generics, cio� definisce il tipo di oggetti che pu� contenere la collezione
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.row,
					R.id.rowText, arrayData) {
						
						@Override
						// l'override serve perch� l'oggetto dell'adapter non � pi� standard e il riempimento del layout � un po' pi� raffinato
						public View getView(int position, View convertView, ViewGroup parent){	
							// Se non presente una istanza della View la creiamo attraverso
							// inflating. Se gi� presente la riutilizziamo (La cella gi� creata sarebbe quella uscita dalla schermata => caching dei dati)
							if(convertView==null){
								// Otteniamo il riferimento alla View da parserizzare
								LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								convertView = inflater.inflate(R.layout.row, null);					
							}

							// Otteniamo i campi da riempire
							TextView text = (TextView)convertView.findViewById(R.id.rowText);

							// Assegnamo i valori
							text.setText(arrayData[position]);
							
							if (position%2 == 0)
								convertView.setBackgroundColor(getResources().getColor(R.color.blue));
							else
								convertView.setBackgroundColor(getResources().getColor(R.color.green));
							
							// Ritorniamo la View
							return convertView;
						}
					};
				
		//ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayData);
		// Lo impostiamo come adapter della listView
		listView.setAdapter(arrayAdapter);
	}
}
