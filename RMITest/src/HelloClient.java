import java.rmi.*;

public class HelloClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Connecting to server...");
		String serverName = "localhost";
		RemoteHello hello = null;
		
		try
		{
			System.setSecurityManager(new RMISecurityManager());
			hello = (RemoteHello) Naming.lookup("rmi://" + serverName + "/hello");
		}
		catch(Exception e)
		{
			System.out.println("Cannot find server " + e.toString());
			System.exit(1);
		}

	}

}
