
public class PatternCheckResource implements Runnable {

	private Thread owner, t2;
	private int counter;
	
	public PatternCheckResource() {
		owner = null;
		counter = 0;

		// secondary thread
		t2 = new Thread(this);
		t2.setName("secondaryThread");
		t2.start();
	}

	private synchronized boolean tryControl()
	{
		Thread me = Thread.currentThread();
		if (owner == null) // no owner set => the resource is free
		{
			owner = me;
			counter = 1;
			return true;
		}
		else if (owner == me) // me already owns the resource
		{
			counter++;
			return true;
		}
		else
			return false; // resource occupied
	}
	
	private synchronized void controlResource()
	{
		while (!tryControl())
		{
			try
			{
				wait();
			}
			catch (InterruptedException ie)
			{}
		}
	}

	private synchronized void releaseResource()
	{
		if (owner == Thread.currentThread())
		{
			counter--;
			if (counter == 0)
			{
				owner = null;
				notify();
			}
		}
	}

	/* debug methods */
	public void run()
	{
		System.out.println("the secondary thread is waiting for the shared resource...");
		controlResource();
		System.out.println("the secondary thread now gets control of the shared resource...");
		System.out.println("The owner is: " + owner.getName());
	}
	
	public static void main(String[] args) {
		PatternCheckResource cr = new PatternCheckResource();
		// the main thread gets control of the shared resource
		System.out.println("the main thread gets control of the shared resource...");
		cr.controlResource();
		System.out.println("The owner is: " + cr.owner.getName());

		System.out.println("releasing resource...");
		cr.releaseResource();
		//System.out.println("resource released by " + Thread.currentThread().getName());
		try
		{
			System.out.println("The owner is: " + cr.owner.getName());
		}
		catch(NullPointerException e)
		{
			System.out.println("The resource has no owner");
		}
	}

}
