import java.rmi.*;
import java.rmi.server.*;
import java.io.*;

public class Hello extends UnicastRemoteObject implements RemoteHello, Serializable {

	private String hello; 
	
	public Hello(String h) throws RemoteException {
		hello = h;
	}

	public String sayHello() throws RemoteException {
		return "Hello " + hello + "!";
	}	
}
