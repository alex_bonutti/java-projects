import java.rmi.*;
import java.rmi.server.*;

public class HelloServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String connectionName = "test1";

		try
		{
			System.setSecurityManager(new RMISecurityManager());
			Hello hello = new Hello(connectionName);
			Naming.rebind(connectionName, hello);
			System.out.println("Hello object ready...");
		}
		catch(Exception e)
		{
			System.out.println("Exception " + e.toString() + "...");
			System.exit(1);
		}
	}

}
