import javax.swing.JOptionPane;


public class TicTacToeGame {

	final private int EMPTY_CELL = 0;
	final private int PLAYER_X = 1;
	final private int PLAYER_O = 2;

	private Player[] players;
	private int[][] playboard;

	public TicTacToeGame() {

		players = new Player[3];
		players[PLAYER_X] = new Player("x");
		players[PLAYER_O] = new Player("o");
		
		playboard = new int[BoardSettings.BOARD_CELLS_DIMENSION][BoardSettings.BOARD_CELLS_DIMENSION];
		int i, j;
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				// board initialization as empty
				playboard[i][j] = EMPTY_CELL;
			}
		}
	}

	public Player getPlayer(int p)
	{
		return players[p];
	}
	
	public int whoseTurn()
    {
    	if (players[PLAYER_X].MyTurn())
    		return PLAYER_X;
    	else if (players[PLAYER_O].MyTurn())
    		return PLAYER_O;
    	
    	return EMPTY_CELL;
    }

	public void changeTurn()
    {
    	checkTie();
    	players[PLAYER_X].changeMyTurn();
    	players[PLAYER_O].changeMyTurn();
    }

	public boolean makeMove (int i, int j, int pl)
    {
    	if (playboard[i][j] == EMPTY_CELL)
    	{
	    	playboard[i][j] = pl;
			return true;
    	}
    	else
    		JOptionPane.showMessageDialog(null, Strings.MSG_OCCUPIED_CELL);
    	
    	return false;
    }
    
	public boolean checkWinner(int pl)
    {
    	return (checkRow(pl) || checkColumn(pl) || checkCrossForward(pl) || checkCrossBackward(pl));
    }
    
	public boolean checkRow(int pl)
    {
    	boolean tictactoe;
		for (int row = 0; row < BoardSettings.BOARD_CELLS_DIMENSION; row++)
		{
			tictactoe = true;
			for (int i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
			{
				if (playboard[row][i] != pl)
					tictactoe = false;
			}
			if (tictactoe)
				return tictactoe;
		}
		
		return false;
    }

	public boolean checkColumn(int pl)
    {
    	boolean tictactoe;
		for (int col = 0; col < BoardSettings.BOARD_CELLS_DIMENSION; col++)
		{
			tictactoe = true;
			for (int i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
			{
				if (playboard[i][col] != pl)
					tictactoe = false;
			}
			if (tictactoe)
				return tictactoe;
		}
		
		return false;
    }

	public boolean checkCrossForward(int pl)
    {
		for (int c = 0; c < BoardSettings.BOARD_CELLS_DIMENSION; c++)
		{
			if (playboard[c][c] != pl)
				return false;
		}

		return true;
    }

	public boolean checkCrossBackward(int pl)
    {
    	int i, j;
		for (i = BoardSettings.BOARD_CELLS_DIMENSION-1, j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; i--, j++)
		{
			if (playboard[i][i] != pl)
				return false;
		}

		return true;
    }

	public void checkTie()
    {
		for (int i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (int j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				if (playboard[i][j] == EMPTY_CELL)
					return;
			}
		}
		
		JOptionPane.showMessageDialog(null, Strings.MSG_TIE);
		System.exit(1);
    }

	public void startGame()
    {
    	players[PLAYER_X].changeMyTurn();
    }

}
