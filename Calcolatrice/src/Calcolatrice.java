import java.awt.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.*;

public class Calcolatrice {

	// layout
	private Frame f;
	GridBagConstraints gdc;
	JPanel p;
	final private int dim = 75;
	
	// elements
	private JTextField t;
	
	// status
	private boolean append;
	private int result;
	private int temp;
	private char currentOperator = 'x';

	public Calcolatrice() {
		// frame
		f = new Frame("Calcolatrice");
		f.setSize(360, 480);
		f.setLocation(200, 200);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				System.exit(1);
			}
		}
		);

		// textfield
		t = new JTextField(20);
		t.setText("0");
		t.setEnabled(false);
		Color fgColor = UIManager.getColor("TextField.foreground");  
		t.setDisabledTextColor(fgColor);
		
		// calcolator status
		append = false;
		result = 0;
		temp = 0;

		// buttons
		NumberButton[] buttons = new NumberButton[10];
		int i;
		for (i = 0; i <= 9; i++)
		{
			buttons[i] = new NumberButton(NumberFormat.getInstance().format(i).toString(), i);
			buttons[i].setPreferredSize(new Dimension(dim, dim));
			buttons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					int v = ((NumberButton) e.getSource()).getValue();
					String number = NumberFormat.getInstance().format(v).toString();
					
					if (append)
						t.setText(t.getText() + number);
					else
						t.setText(number);

					// after a digit, the append becomes always true
					append = true;
				}
			});
		}

		String[] operations_signs = {"+", "-", "*", ":"};
		OperatorButton[] operations = new OperatorButton[4];
		for (i = 0; i < 4; i++)
		{
			operations[i] = new OperatorButton(operations_signs[i]);
			operations[i].setPreferredSize(new Dimension(dim, dim));
			operations[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					// last operation
					doLastOperation();

					// current operation and update/display result
					String o = ((OperatorButton) e.getSource()).getOperator();
					currentOperator = o.charAt(0);
					setCurrentStatus();
				}
			});
		}

		OperatorButton equals = new OperatorButton("=");
		equals.setPreferredSize(new Dimension(dim, dim));
		equals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				// last operation
				doLastOperation();

				// display result
				String o = ((OperatorButton) e.getSource()).getOperator();
				currentOperator = o.charAt(0);
				setCurrentStatus();
			}
		});

		OperatorButton canc = new OperatorButton("C");
		canc.setPreferredSize(new Dimension(dim, dim));
		canc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				String o = ((OperatorButton) e.getSource()).getOperator();
				currentOperator = o.charAt(0);
				setCurrentStatus();
			}
		});

		// raggruppa elementi grafici
		gdc = new GridBagConstraints();
		gdc.weightx = 100;
		gdc.weighty = 100;
		gdc.fill = 100;
		
		p = new JPanel();
		p.setLayout(new GridBagLayout());
		
		// text
		addButton(0, 0, 4, 1);
		p.add(t, gdc);

		// 7 8 9
		addButton(0, 1, 1, 1);
		p.add(buttons[7], gdc);

		addButton(1, 1, 1, 1);
		p.add(buttons[8], gdc);

		addButton(2, 1, 1, 1);
		p.add(buttons[9], gdc);

		// 4 5 6
		addButton(0, 2, 1, 1);
		p.add(buttons[4], gdc);

		addButton(1, 2, 1, 1);
		p.add(buttons[5], gdc);

		addButton(2, 2, 1, 1);
		p.add(buttons[6], gdc);

		// 1 2 3
		addButton(0, 3, 1, 1);
		p.add(buttons[1], gdc);

		addButton(1, 3, 1, 1);
		p.add(buttons[2], gdc);

		addButton(2, 3, 1, 1);
		p.add(buttons[3], gdc);

		// operation
		for (i = 0; i < 4; i++)
		{
			addButton(3, i+1, 1, 1);
			p.add(operations[i], gdc);
		}
		
		// 0
		addButton(1, 4, 1, 1);
		p.add(buttons[0], gdc);

		// =
		addButton(2, 4, 1, 1);
		p.add(equals, gdc);

		// canc
		addButton(0, 4, 1, 1);
		p.add(canc, gdc);

		// rendiamo visibili nel frame tutti gli elementi
		f.add(p);

		f.setVisible(true);
	}
	
	private void addButton(int gridx, int gridy, int gridwidth, int gridheight)
	{
		gdc.gridx = gridx;
		gdc.gridy = gridy;
		gdc.gridwidth = gridwidth;
		gdc.gridheight = gridheight;
	}

	private void doLastOperation()
	{
		switch (currentOperator)
		{
			case 'x':
			// case 'C' not possible
				temp += Integer.parseInt(t.getText());
				result = temp;
				break;
			
			case '+':
				result = temp + Integer.parseInt(t.getText());
				break;

			case '-':
				result = temp - Integer.parseInt(t.getText());
				break;

			case '*':
				result = temp * Integer.parseInt(t.getText());
				break;

			case ':':
				result = temp / Integer.parseInt(t.getText());
				break;

			case '=':
				resetResult();
				break;

			default:
				break;
		}
		// update result with the temp value
		temp = result;
	}

	private void setCurrentStatus()
	{
		String number;
		switch (currentOperator)
		{
			case '+':
			case '-':
			case '*':
			case ':':
				append = false;
				number = NumberFormat.getInstance().format(result).toString();
				t.setText(number);
				break;

			case '=':
				append = false;
				number = NumberFormat.getInstance().format(result).toString();
				t.setText(number);
				resetResult();
				currentOperator = 'x';
				break;

			case 'C':
				append = false;
				resetResult();
				t.setText("0");
				currentOperator = 'x';
				break;

			default:
				break;
		}
	}	
	
	private void resetResult()
	{
		result = temp = 0;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calcolatrice calc = new Calcolatrice();
	}

}
