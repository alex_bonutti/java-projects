package com.mobe.dati;

import android.content.ContentValues;
import android.database.Cursor;

public class TableProducts {
	public static String TABLE_NAME = "Books";
	
	public static String ID_PRODUCT = "id_product";
	public static String AUTHOR = "author";
	public static String NAME = "name";


	public static String[] COLUMNS = new String[] { ID_PRODUCT, AUTHOR, NAME};
	
	
	/**
	 * Query to create the table in the DB
	 * @return
	 */
	public static String getQueryCreateTable() 	{
		StringBuilder createQuery = new StringBuilder();
		createQuery.append("CREATE TABLE " + TABLE_NAME + " (");
		createQuery.append("	    " + ID_PRODUCT + " INTEGER PRIMARY KEY AUTOINCREMENT,");
		createQuery.append("	    " + AUTHOR + " TEXT,");
		createQuery.append("	    " + NAME + " TEXT");
		createQuery.append(")");
		return createQuery.toString();
	}

	public static String insertInitialData() 	{
		StringBuilder createQuery = new StringBuilder();
		createQuery.append("insert into " + TABLE_NAME + " (" + AUTHOR + ", " + NAME + ") values ('Dan Brown', 'Il codice Da Vinci')");
		return createQuery.toString();
	}
	
	/**
	 * Convert the instance in a ContentValues object
	 * @return
	 */
	public static ContentValues toContentValues(Product p) {
		ContentValues values = new ContentValues();
		values.put(ID_PRODUCT, p.getId());
		values.put(AUTHOR, p.getAuthor());
		values.put(NAME, p.getName());
		return values;
	}
	
	
	
	/**
	 * Create an instance from a give Cursor
	 * @return
	 */
	public static Product fromCursor(Cursor cur) {
		Product s =  new Product(
				cur.getInt(cur.getColumnIndex(ID_PRODUCT)),
				cur.getString(cur.getColumnIndex(AUTHOR)),
				cur.getString(cur.getColumnIndex(NAME)));
		return s;
	}
	
	
}
