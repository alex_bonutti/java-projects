
public class BoardSettings {

	final static public int dimBoard = 480;
	final static public int boardPosition = 200;
	final static public int dimCell = 120;
	final static public int BOARD_CELLS_DIMENSION = 3;

}
