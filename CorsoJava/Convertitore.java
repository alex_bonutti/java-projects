import java.awt.*;
import java.awt.event.*;
import java.text.*;

public class Convertitore implements ActionListener {

	private final fattoreConversione = 1936.27;
	private Frame f;
	private TextField t;
	private Checkbox lire, euro;
	private Button b;
	
	public Convertitore() {
		// frame
		f = new Frame("Convertitore �/�");
		f.setSize(160, 100);
		f.setLocation(200, 200);
		
		f.addWindowAdapter(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				System.exit(1);
			}
		}
		);
		
		// textfield
		t = new TextField("");
		
		// button
		b = new Button("converti");
		// serve per aggiungere i metodi della classe corrente alle azioni del bottone 
		b.addActionListener(this);
		
		// checkbox
		CheckboxGroup cbg = new CheckboxGroup();
		lire = new Checkbox("�", true, cbg);
		euro = new Checkbox("�", false, cbg);
		
		// raggruppa elementi grafici
		Panel p = new Panel();
		p.add(lire);
		p.add(euro);
		p.add(b);

		// rendiamo visibili nel frame tutti gli elementi
		f.add(t, "North");
		f.add(p, "South");
		f.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Convertitore c = new Convertitore();
	}

}
