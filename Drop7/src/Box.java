import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;


public class Box extends JButton {

	int x;
	int y;
	
	public Box(int i, int j) {

		setPreferredSize(new Dimension(BoardSettings.dimCell, BoardSettings.dimCell));
		setBackground(Color.BLACK);
		
		x = i;
		y = j;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setPawn(String img)
	{
		ImageIcon temp = new ImageIcon(img);
		setIcon(temp);
	}
}
