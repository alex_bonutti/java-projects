import java.awt.*;

public class NumberButton extends Button {
	private int value;
	
	public NumberButton(String label, int v) {
		super(label);
		value = v;
	}
	
	public void setNumberButton(int v) {
		value = v;
	}
	
	public int getValue() {
		return value;
	}
}
