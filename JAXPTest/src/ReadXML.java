import javax.xml.parsers.*;  
  
public class ReadXML {  

	public void getXml(XmlHandler xmlHandler, String xml_file) {
		try {  
			// obtain and configure a SAX based parser  
   			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();  

   			// obtain object for SAX parser  
   			SAXParser saxParser = saxParserFactory.newSAXParser();     			
	  
   			// parse the XML specified in the given path and uses supplied  
   			// handler to parse the document  
   			// this calls startElement(), endElement() and character() methods  
   			// accordingly  
   			saxParser.parse(xml_file, xmlHandler);
		}
		catch (Exception e) {
			e.printStackTrace();  
		} 
	}
}

