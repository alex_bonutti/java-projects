import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

public class TicTacButton extends JButton {

	int x;
	int y;
	
	public TicTacButton(int i, int j) {

		setPreferredSize(new Dimension(BoardSettings.dimCell, BoardSettings.dimCell));
		setBackground(Color.WHITE);
		
		x = i;
		y = j;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setPawn(String img)
	{
		ImageIcon temp = new ImageIcon(img);
		setIcon(temp);
	}
}
