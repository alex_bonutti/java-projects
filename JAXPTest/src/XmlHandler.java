import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XmlHandler extends DefaultHandler
{
	String firstNameTag="close";  
	String lastNameTag="close";  
	String emailTag="close";  
	String phoneTag="close";  

	// store data
	private int current_index = -1;
	private Student[] students;

	public XmlHandler(int n) {
		students = new Student[n];
	}

	public Student getStudent(int index) {
		return students[index];
	}
	
	// this method is called every time the parser gets an open tag '<'  
	// identifies which tag is being open at time by assigning an open flag  
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("STUDENT")) {  
			current_index++;
			students[current_index] = new Student(attributes.getValue("id"));
		}
		if (qName.equalsIgnoreCase("FIRSTNAME")) {  
			firstNameTag = "open";  
		}
		if (qName.equalsIgnoreCase("LASTNAME")) {  
	   		lastNameTag = "open";  
		}
		if (qName.equalsIgnoreCase("EMAIL")) {  
			emailTag = "open";  
		}
		if (qName.equalsIgnoreCase("PHONE")) {  
			phoneTag = "open";  
		}
	}  
	  
	// prints data stored in between '<' and '>' tags  
	public void characters(char ch[], int start, int length) throws SAXException {

		if (firstNameTag.equals("open")) {  
			students[current_index].setFirstName(new String(ch, start, length));
		}  
		if (lastNameTag.equals("open")) {  
			students[current_index].setLastName(new String(ch, start, length));
		}  
		if (emailTag.equals("open")) {
			students[current_index].setEmail(new String(ch, start, length));
		}
		if (phoneTag.equals("open")) {  
			students[current_index].setPhone(new String(ch, start, length));
		}
	}
	  
	// calls by the parser whenever '>' end tag is found in xml
	// makes tags flag to 'close'  
	public void endElement(String uri, String localName, String qName) throws SAXException {
	 
		if (qName.equalsIgnoreCase("firstName")) {  
			firstNameTag = "close";  
		}
		if (qName.equalsIgnoreCase("lastName")) {  
			lastNameTag = "close";  
		}
		if (qName.equalsIgnoreCase("email")) {  
	   		emailTag = "close";  
		}
		if (qName.equalsIgnoreCase("phone")) {  
	   		phoneTag = "close";  
		}
	}
}
