
public class Strings {

	final static public String GAME_TITLE = "TicTacToe";
	final static public String MSG_OCCUPIED_CELL = "Casella gi� occupata!";
	final static public String MSG_WINNER = "The winner is Player: ";
	final static public String MSG_TIE = "The game ended with a tie!";
	
}
