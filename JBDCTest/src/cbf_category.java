import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class cbf_category extends JDBCTest {
	
	String id;
	String type;
	String description;
	
	public cbf_category() throws Exception {
		super();

		table = "cbf_category";
		primary_key = "id";
	}

	/* override of the parent method */
	public boolean fetch()
	{
		try {
			if (r.next())
			{
				id = r.getString("id");
				type = r.getString("type");
				description = r.getString("description");
				
				return true;
			}
		}
		catch (SQLException e) {}

		return false;
	}

	public int insert() throws Exception
	{
		String query = "insert into " + table + " (type, description) values (?, ?)";
		
		insertStatement = c.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		//insertStatement.setString(1, id);
		insertStatement.setString(1, type);
		insertStatement.setString(2, description);		
        if (insertStatement.executeUpdate() > 0) {
        	r = insertStatement.getGeneratedKeys();
        	if (r.next())
        		return r.getInt(primary_key);
        }
        
        return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		cbf_category obj = new cbf_category();
		
		//System.out.println("TransactionIsolation: " + obj.getTransactionIsolation());
		
		// static get
		if (obj.staticGet("4"))
		{
			System.out.println("staticGet: " + obj.id + " " + obj.description);			
		}
		
		// delete
		if (obj.staticGet("6"))
		{
			System.out.println("deleting... " + obj.id + " " + obj.description);
			obj.delete();
		}
		
		// test insert
		int newID;
		obj.type = "t";
		obj.description = "TEST";
		if ((newID = obj.insert()) != 0)
		{
			System.out.println("fetch " + newID + " " + obj.description);			
		}
		
		obj.find();
		while (obj.fetch())
		{
			System.out.println("fetch " + obj.id + " " + obj.description);
			System.out.println("getField " + obj.getField("id") + " " + obj.getField("description"));
		}
		obj.releaseResource();
	}

}
