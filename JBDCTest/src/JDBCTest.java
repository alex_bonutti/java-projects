import java.sql.*;

public class JDBCTest {

	// connection params
	String JDBCDriverName = "com.mysql.jdbc.Driver";
	String databaseURL = "jdbc:mysql://localhost/cibieffe";
	String DBuser = "test";
	String DBpsw = "timetabling";

	// java.sql objects
	Connection c;
	Statement sql;
	PreparedStatement insertStatement;
	ResultSet r;
	
	// db table fields
	String table;
	String primary_key;
	String[] fields;
	
	public JDBCTest() throws Exception {
		init();
		
		String query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + table + "'";
		doQuery(query);
		int i = 0;
		while (r.next())
		{
			fields[i] = r.getString(i);
		}
	}

	public void init() {
		try {
			Class.forName(JDBCDriverName);
			
			c = DriverManager.getConnection(databaseURL, DBuser, DBpsw);
				
			// create Statement
			sql = c.createStatement();
		}
		catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver " + JDBCDriverName + " not found!");
		}
		catch (SQLException e) {
			System.out.println("Connection to " + databaseURL + " with " + DBuser + "/" + DBpsw + " not possible!");
		}
	}

	public void setAutoCommit(boolean commit)
	{
		try {
			c.setAutoCommit(commit);
		}
		catch (SQLException e) {};
	}

	public void setTransactionIsolation(int i)
	{
		try {
			c.setTransactionIsolation(i);
		}
		catch (SQLException e) {}
	}	

	public int getTransactionIsolation()
	{
		try {
			return c.getTransactionIsolation();
		}
		catch (SQLException e) {
			return Connection.TRANSACTION_NONE;
		}
	}	
	
	public void doQuery(String query) {
		try {
			r = sql.executeQuery(query);
		}
		catch (SQLException e)
		{
			//System.out.println("Query " + query + " failed!");
		}
	}

	public int doUpdate(String query) {
		try {
			return sql.executeUpdate(query);
		}
		catch (SQLException e)
		{
			System.out.println("Query " + query + " failed!");
			return 0;
		}
	}

	/* all of the fields are read as Strings */
	public String getField(String field)
	{
		try {
			return r.getString(field);
		}
		catch (SQLException e) {}

		return "";
	}

	public void releaseResource() {
		try {
			sql.close();
		}
		catch (SQLException e)
		{
			//System.out.println("releaseResource() not possible!");
		}
	}

	public void find()
	{
		String query = "select * from " + table;
		doQuery(query);		
	}
	
	public boolean fetch()
	{
		try {
			return r.next();
		}
		catch (SQLException e) {}

		return false;
	}

	public boolean staticGet(String key)
	{
		String query = "select * from " + table + " where " + primary_key + " = '" + key + "'";
		
		doQuery(query);
		return fetch();
	}

	public int delete()
	{
		try {
			String query = "delete from " + table + " where " + primary_key + " = '" + r.getString(primary_key) + "'";
			return doUpdate(query);
		}
		catch (Exception e) {}

		return 0;
	}

	/*
	public static void main(String[] args) {

		JDBCTest jdbcTest = new JDBCTest();

		String query;
		try {
			query = "insert into cbf_brand (name) values ('jdbc2')";
			System.out.println("affected rows: " + jdbcTest.doUpdate(query));

			query = "select * from cbf_brand";
			ResultSet res = jdbcTest.doQuery(query);
			while (res.next())
			{
				System.out.println(res.getInt("id") + " => " + res.getString("name"));				
			}

			jdbcTest.releaseResource();
		}
		catch (SQLException e)
		{
			System.out.println(e.getMessage());
		}
		catch (NullPointerException e)
		{
		}
	}*/
}
