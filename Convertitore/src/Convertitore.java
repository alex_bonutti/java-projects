import java.awt.*;
import java.awt.event.*;
import java.text.*;

public class Convertitore implements ActionListener {

	private final double fattoreConversione = 1936.27;
	private Frame f;
	private Dialog a;
	private TextField t;
	private Checkbox lire, euro;
	private Button b, c1, c2;
	
	public Convertitore() {
		// frame
		f = new Frame("Convertitore �/�");
		f.setSize(320, 150);
		f.setLocation(200, 200);
		
		// WindowAdapter e' gia' un'implementazione di WindowListener, che ha 6 metodi da implementare, e su questa implementiamo solo la funzione di chiusura che ci interessa
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				closeConvertitore();
			}
		}
		);

		// textfield
		t = new TextField("");

		// button
		b = new Button("converti");
		// serve per aggiungere i metodi della classe corrente alle azioni del bottone 
		b.addActionListener(this);
		
		// checkbox
		CheckboxGroup cbg = new CheckboxGroup();
		lire = new Checkbox("�", true, cbg);
		euro = new Checkbox("�", false, cbg);
		
		// raggruppa elementi grafici
		Panel p = new Panel();
		p.add(lire);
		p.add(euro);
		p.add(b);

		// button
		c1 = new Button("chiudi via bottone");
		c1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				closeConvertitore();
			}
		}
		);
		c2 = new Button("chiudi via convertitore");
		c2.addActionListener(this);
		Panel cc = new Panel();
		cc.add(c1);
		cc.add(c2);

		// rendiamo visibili nel frame tutti gli elementi
		f.add(t, "North");
		f.add(p, "Center");
		f.add(cc, "South");
		f.setVisible(true);
	}

	public void closeConvertitore()
	{
		// dialog
		a = new Dialog(a, "Sei sicuro?", false);
		a.setSize(150, 150);
		a.setLocation(250, 250);
		a.setVisible(true);
		//if (a.getC)
		System.exit(1);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == c2)
		{
			closeConvertitore();
		}
		else
		{
			String valore = t.getText();
			double d;
			try
			{
				d = NumberFormat.getInstance().parse(valore).doubleValue();
			}
			catch (ParseException pe)
			{
				d = 0.0;
			}
			
			if (lire.getState())
			{
				d = d/fattoreConversione;
				euro.setState(true);
			}
			else
			{
				d = d*fattoreConversione;
				lire.setState(true);
			}
			
			t.setText(NumberFormat.getInstance().format(d).toString());
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Convertitore c = new Convertitore();
	}
}
