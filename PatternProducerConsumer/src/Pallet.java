
public class Pallet {
	
	private int resource;
	private boolean available = false;

	// get the shared resource when available
	public synchronized int get() {
		while (!available)
		{
			try
			{
				// the thread waits until the resource becomes available
				wait();
			}
			catch (InterruptedException ie) {}
		}
		
		available = false;
		notify();
		return resource;
	}

	// put a new resource when the shared resource is no more available
	public synchronized void put(int r) {
		while (available)
		{
			try
			{
				wait();
			}
			catch (InterruptedException ie) {}
		}
		
		resource = r;
		available = true;
		notify();
	}
}
