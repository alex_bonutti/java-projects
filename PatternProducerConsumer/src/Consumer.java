
public class Consumer extends Thread {

	private Pallet pallet;

	public Consumer(Pallet p, String name) {
		pallet = p;
		setName(name);
	}

	public void run()
	{
		int r;
		for (int i = 0; i < 10; i++)
		{
			r = pallet.get();
			System.out.println("consumer " + getName() + " got resource " + r + "...");
		}
	}
}
