package com.mobe.ial;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorActivity extends Activity {
	
	int result = 0;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final TextView display = (TextView) findViewById(R.id.display);

        Button button0 = (Button) findViewById(R.id.button0);
		button0.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
	
					if (display.getText() != "0")
						display.append("0");
					
				}
	
			});

	    Button button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(new OnClickListener() {
				public void onClick(View arg0) {
	
					if (display.getText() == "0")
						display.setText("1");
					else
						display.append("1");					
				}
	
			});

    Button button2 = (Button) findViewById(R.id.button2);
	button2.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				if (display.getText() == "0")
					display.setText("2");
				else
					display.append("2");					
			}

		});


Button button3 = (Button) findViewById(R.id.button3);
button3.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("3");
			else
				display.append("3");					
		}

	});


Button button4 = (Button) findViewById(R.id.button4);
button4.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("4");
			else
				display.append("4");					
		}

	});

Button button5 = (Button) findViewById(R.id.button5);
button5.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("5");
			else
				display.append("5");					
		}

	});

Button button6 = (Button) findViewById(R.id.button6);
button6.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("6");
			else
				display.append("6");					
		}

	});


Button button7 = (Button) findViewById(R.id.button7);
button7.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("7");
			else
				display.append("7");					
		}

	});


Button button8 = (Button) findViewById(R.id.button8);
button8.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("8");
			else
				display.append("8");					
		}

	});


Button button9 = (Button) findViewById(R.id.button9);
button9.setOnClickListener(new OnClickListener() {
		public void onClick(View arg0) {

			if (display.getText() == "0")
				display.setText("9");
			else
				display.append("9");					
		}

	});

    Button buttonPlus = (Button) findViewById(R.id.buttonPlus);
    buttonPlus.setOnClickListener(new OnClickListener() {
    		public void onClick(View arg0) {

    			result += Integer.parseInt(display.getText().toString());
    			display.setText("");
    			
    		}

    	});

    Button buttonResult = (Button) findViewById(R.id.buttonResult);
    buttonResult.setOnClickListener(new OnClickListener() {
    		public void onClick(View arg0) {

    			result += Integer.parseInt(display.getText().toString());
    			display.setText("" + result);
    			
    		}

    	});
    }

}

/*    public int returnButtonId(int i)
    {
    	switch(i)
    	{
    		case 0:
    			return R.id.button0;
    		case 1:
    			return R.id.button1;
    		case 2:
    			return R.id.button2;
    		case 3:
    			return R.id.button3;
    		case 4:
    			return R.id.button4;
    		case 5:
    			return R.id.button5;
    		case 6:
    			return R.id.button6;
    		case 7:
    			return R.id.button7;
    		case 8:
    			return R.id.button8;
    		case 9:
    			return R.id.button9;
    		default:
    			return 0;
    	}
    }*/