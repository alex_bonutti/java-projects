
public class Player {

	String id;
	boolean turnOf;

	public Player(String s) {
		id = s;
		turnOf = false;
	}

	public String Id() {
		return id;
	}

	public String Pawn() {
		return id+".png";
	}

	public void changeMyTurn() {
		turnOf = !turnOf;
	}

	public boolean MyTurn() {
		return turnOf;
	}
}
