import java.awt.Button;

public class OperatorButton extends Button {
	private String operator;
	
	public OperatorButton(String o) {
		super(o);
		this.setSize(40, 40);
		operator = o;
	}
	
	public void setOperator(String o) {
		operator = o;
	}
	
	public String getOperator() {
		return operator;
	}
}