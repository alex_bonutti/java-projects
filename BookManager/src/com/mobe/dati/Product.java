package com.mobe.dati;

public class Product {
	private Integer id;
	private String author;
	private String name;
	
	
	public Product(int id, String author, String name) {
		this.id = id;
		this.author = author;
		this.name = name;
	}
	
	public Product(String author, String title) {
		this.author = author;
		this.name = title;
		this.id = null;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	
}
