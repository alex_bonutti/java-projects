package it.apogeo.android.cap05.arrayadaptertest;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ArrayAdapterTestActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Impostiamo il Layout
		setContentView(R.layout.main);
		
		// Otteniamo il riferimento alla ListView
		final ListView listView = (ListView) findViewById(R.id.arrayList);

		// Otteniamo il riferimento all'array dei dati
//		final String[] arrayData = getResources().getStringArray(R.array.array_data); 
		final ArrayList<String> arrayData = new ArrayList<String>();
		arrayData.add("Alex");
		arrayData.add("Panino");
		arrayData.add("Luca");
		
		// Creiamo l'ArrayAdapter => crea tanti elementi R.layout.row quante sono le stringhe in arrayData
		// NB: ArrayAdapter � una collezione di oggetti, che possono essere di diverso tipo.
		// <String> � un generics, cio� definisce il tipo di oggetti che pu� contenere la collezione
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.row, R.id.rowText, arrayData) {
						
						@Override
						// l'override serve perch� l'oggetto dell'adapter non � pi� standard e il riempimento del layout � un po' pi� raffinato
						public View getView(int position, View convertView, ViewGroup parent){	
							// Se non presente una istanza della View la creiamo attraverso
							// inflating. Se gi� presente la riutilizziamo (La cella gi� creata sarebbe quella uscita dalla schermata => caching dei dati)
							if(convertView==null){
								// Otteniamo il riferimento alla View da parserizzare
								LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								convertView = inflater.inflate(R.layout.row, null);					
							}

							// Otteniamo i campi da riempire
							TextView text = (TextView)convertView.findViewById(R.id.rowText);

							// Assegnamo i valori
							text.setText(arrayData.get(position));
							
							ImageView img = (ImageView)convertView.findViewById(R.id.delete_icon);
							
							
							
							// Ritorniamo la View
							return convertView;
						}
					};
				
		//ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayData);
		// Lo impostiamo come adapter della listView
		listView.setAdapter(arrayAdapter);

	    listView.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3) 
				{
									
					AlertDialog.Builder builder = new AlertDialog.Builder(ArrayAdapterTestActivity.this);
					builder.setTitle("titolo dialog");
					//builder.setIcon(R.drawable.ic_launcher);
					builder.setMessage("cancello l'elemento " + position + "?");
					builder.setPositiveButton("si",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									
									String a = arrayAdapter.getItem(position);
									arrayAdapter.remove(a);
									
									dialog.dismiss();
								}
							});
					
					builder.setNegativeButton("no",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									
									dialog.dismiss();
								}
							});

					AlertDialog alert = builder.create();
					alert.show();
				}
		});

	}

	public void onClickWhatToDo(View view, int pos) {
		// Dialog modale (possiamo modificare anche qui il layout)
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("titolo dialog");
		//builder.setIcon(R.drawable.ic_launcher);
		builder.setMessage("cancello l'elemento " + (pos+1) + "?");
		builder.setPositiveButton("si",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.dismiss();
					}
				});
		
		builder.setNegativeButton("no",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						dialog.dismiss();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
	}

}
