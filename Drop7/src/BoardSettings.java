
public class BoardSettings {

	final static public int dimBoardL = 480;
	final static public int dimBoardH = 540;
	final static public int boardPosition = 200;
	final static public int dimCell = 60;
	final static public int BOARD_CELLS_DIMENSION = 7;
	final static public int BROKEN_PAWN_ID = BOARD_CELLS_DIMENSION+1;
	final static public int FULL_PAWN_ID = BOARD_CELLS_DIMENSION+2;
	final static public int PAWNS_DIMENSION = FULL_PAWN_ID+1;

}
