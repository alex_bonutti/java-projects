import java.rmi.*;

public interface RemoteHello {

	String sayHello() throws RemoteException;
	
}
