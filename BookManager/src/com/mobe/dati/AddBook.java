package com.mobe.dati;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddBook extends Activity {

    private DbManager dbManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.form);
        dbManager = new DbManager(this);

        final EditText author = (EditText) findViewById(R.id.edit_author);
        final EditText title = (EditText) findViewById(R.id.edit_title);
        
		Button addButton = (Button) findViewById(R.id.add_button);
		addButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

		    	Product p = new Product(author.getText().toString(), title.getText().toString());
		    	dbManager.addProduct(p);
			}

		});
    
    
		Button backButton = (Button) findViewById(R.id.back_button);
		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				// Andiamo in modo esplicito alla prima act
				Intent intent1 = new Intent(AddBook.this,
						PersistenzaDatiActivity.class);
				startActivity(intent1);
			}

		});

    }
	
}
