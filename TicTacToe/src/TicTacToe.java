import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TicTacToe {

	// layout
	private Frame f;
	private GridBagConstraints gdc;
	private Panel p;

	// game
	private TicTacToeGame game;
	
	public TicTacToe() {
		
		// game
		game = new TicTacToeGame();
		
		// layout
		f = new Frame(Strings.GAME_TITLE);
		f.setSize(BoardSettings.dimBoard, BoardSettings.dimBoard);
		f.setLocation(BoardSettings.boardPosition, BoardSettings.boardPosition);
		f.setBackground(Color.BLACK);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				System.exit(1);
			}
		} );

		f.setResizable(false);

		gdc = new GridBagConstraints();
		gdc.weightx = 100;
		gdc.weighty = 100;
		gdc.fill = 100;

		// BackgroundPanel p = new BackgroundPanel();
		p = new Panel();
		p.setLayout(new GridBagLayout());

		int i, j;
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				// layout
				TicTacButton b = new TicTacButton(i, j);
				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						int pl = game.whoseTurn();
						int x = ((TicTacButton) e.getSource()).getX();
						int y = ((TicTacButton) e.getSource()).getY();
						if (game.makeMove(x, y, pl))
						{
							((TicTacButton) e.getSource()).setPawn(game.getPlayer(pl).Pawn());
							if (game.checkWinner(pl))
							{
					    		JOptionPane.showMessageDialog(null, Strings.MSG_WINNER + game.getPlayer(pl).Id());
					    		System.exit(1);
							}
							else
								game.changeTurn();
						}
					}
				});
				addButton(i, j, 1, 1);
				p.add(b, gdc);
			}
		}

		// rendiamo visibili nel frame tutti gli elementi
		f.add(p);
		f.setVisible(true);
	}
 
    private void addButton(int gridx, int gridy, int gridwidth, int gridheight)
    {
    	gdc.gridx = gridx;
    	gdc.gridy = gridy;
    	gdc.gridwidth = gridwidth;
    	gdc.gridheight = gridheight;
    }

    private void startGame()
    {
    	game.startGame();
    }
    
    /**
    * @param args
    */
    public static void main(String[] args)
    {
    	// TODO Auto-generated method stub
    	TicTacToe board = new TicTacToe();
    	board.startGame();
    }
}

