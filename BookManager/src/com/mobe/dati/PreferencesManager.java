package com.mobe.dati;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {
	private static final String DATA = "TESTO";
	
	
	public static void saveToPreferences(Context context, String text) {		
    	SharedPreferences.Editor editor = context.getSharedPreferences(DATA, Context.MODE_PRIVATE).edit();
    	editor.putString(DATA, text);
    	editor.commit();
	}
	
	public static String getFromPreferences(Context context) {
		 return context.getSharedPreferences(DATA, Context.MODE_PRIVATE).getString(DATA, "<vuoto>");
	}
}
