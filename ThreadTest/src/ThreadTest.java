import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ThreadTest implements Runnable {

	int id = 0;
	Frame f;

	/* la classe Thread riceve un oggetto di tipo Runnable, ed esegue
	 * le istruzioni nel metodo run() una volta che l'oggetto Thread
	 * � stato fatto partite lanciando il metodo run()
	 */
	public ThreadTest() {

		// frame
		f = new Frame("Frame " + Thread.currentThread().getName());
		f.setSize(320, 150);
		f.setLocation(200, 200);
		
		// WindowAdapter e' gia' un'implementazione di WindowListener, che ha 6 metodi da implementare, e su questa implementiamo solo la funzione di chiusura che ci interessa
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				closeFrame();
			}
		}
		);
		
		Thread t = new Thread(this);
		t.setName("secondaryThread");
		t.start();
		
		//eseguiAttivita(1);
		//while (id != 0);
		//System.out.println("Waiting...");
		
		eseguiAttivita(1);
		f.setVisible(true);
	}
	
	public void run() {
		while (id != 0);
		eseguiAttivita(2);
	}
	
	public void closeFrame()
	{
		System.exit(1);
	}

	private synchronized void eseguiAttivita(int i) {
		
		id = i;
		f.setName("Frame " + Thread.currentThread().getName() + " (" + i + ")");
		
		// button
		Button b = new Button(i + " => next Thread");
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				id = 0;
				f.remove( (Button) e.getSource() );
			}
		});
		
		f.add(b);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ThreadTest tt = new ThreadTest();
	}

}
