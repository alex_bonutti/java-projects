import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.border.LineBorder;

public class Drop7 implements ActionListener {

	// layout
	private Frame f;
	private GridBagConstraints gdc;
	private Panel p;
	private int nextX;
	private int nextY;
	private Box nextPawn;
	Label lp, ld2;

	// game
	private Box[][] board;
	private Game game;
	
	public Drop7() {
		
		// game
		game = new Game();
		
		// layout
		f = new Frame(Strings.GAME_TITLE);
		f.setSize(BoardSettings.dimBoardL, BoardSettings.dimBoardH);
		f.setLocation(BoardSettings.boardPosition, BoardSettings.boardPosition);
		f.setBackground(Color.WHITE);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				System.exit(1);
			}
		} );

		f.setResizable(false);

		gdc = new GridBagConstraints();
		gdc.weightx = 100;
		gdc.weighty = 100;
		gdc.fill = 100;

		// BackgroundPanel p = new BackgroundPanel();
		p = new Panel();
		p.setLayout(new GridBagLayout());

		int i, j, gx, gy;
		board = new Box[BoardSettings.BOARD_CELLS_DIMENSION][BoardSettings.BOARD_CELLS_DIMENSION];
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				// layout
				Box b = new Box(i, j);
				b.addActionListener(this);
				
				board[i][j] = b;

				gx = j;
				gy = BoardSettings.BOARD_CELLS_DIMENSION - 1 - i;
				addGDCElement(gx, gy, 1, 1);
				p.add(board[i][j], gdc);
			}
		}

		// points
		j = BoardSettings.BOARD_CELLS_DIMENSION;
		addGDCElement(0, j, 1, 1);
		lp = new Label(Integer.toString(game.getPoints()));
		p.add(lp, gdc);

		// next drop
		addGDCElement(1, j, 2, 1);
		Label ld1 = new Label(Strings.MSG_DROP);
		p.add(ld1, gdc);

		addGDCElement(3, j, 1, 1);
		ld2 = new Label(Integer.toString(game.getNextDrop()));
		p.add(ld2, gdc);

		// next pawn
		i = i - 3;
		j = BoardSettings.BOARD_CELLS_DIMENSION;
		addGDCElement(i, j, 2, 1);
		Label ln = new Label(Strings.MSG_NEXT);
		p.add(ln, gdc);

		nextX = i + 2;
		nextY = j;
		addGDCElement(nextX, nextY, 1, 1);
		nextPawn = new Box(nextX, nextY);
		p.add(nextPawn, gdc);
		
		// rendiamo visibili nel frame tutti gli elementi
		f.add(p);
		f.setVisible(true);
	}
	
    private void addGDCElement(int gridx, int gridy, int gridwidth, int gridheight)
    {
    	gdc.gridx = gridx;
    	gdc.gridy = gridy;
    	gdc.gridwidth = gridwidth;
    	gdc.gridheight = gridheight;
    }

	public void actionPerformed(ActionEvent e)
	{
		int p = game.nextPawn();
		//int x = ((Box) e.getSource()).getX();
		int x, y = ((Box) e.getSource()).getY();
		if (game.makeMove(y, p))
		{
			game.updateNextDrop();
			x = game.getLevel();
			redrawGrid();
			game.checkGrid(y);
			game.checkDrop();

			//JOptionPane.showMessageDialog(null, Strings.MSG_WINNER + game.getPawn(p).Id());
	    	//System.exit(1);
			//borderGrid();
			
			/*try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
			
			redrawGrid();

			game.nextMove();
	    	p = game.nextPawn();
	    	nextPawn.setPawn(game.getPawn(p).Pawn());
	    	
	    	lp.setText(Integer.toString(game.getPoints()));
	    	ld2.setText(Integer.toString(game.getNextDrop()));
		}
	}

    private void redrawGrid()
    {
		int i, j, p;
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				p = game.Playboard(i, j);
				if (p != 0)
					board[i][j].setPawn(game.getPawn(p).Pawn());
				else
					board[i][j].setPawn("");
			}
		}
    }

    private void borderGrid()
    {
		int i, j;
		for (i = 0; i < BoardSettings.BOARD_CELLS_DIMENSION; i++)
		{
			for (j = 0; j < BoardSettings.BOARD_CELLS_DIMENSION; j++)
			{
				if (game.toBeRemoved(i, j))
					board[i][j].setBorder(new LineBorder(Color.YELLOW, 10));
			}
		}
    }

    private void startGame()
    {
    	game.startGame();
    	
    	int p = game.nextPawn();
    	nextPawn.setPawn(game.getPawn(p).Pawn());
    }
    
    /**
    * @param args
    */
    public static void main(String[] args)
    {
    	// TODO Auto-generated method stub
    	Drop7 board = new Drop7();
    	board.startGame();
    }
}
