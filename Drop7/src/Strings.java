
public class Strings {

	final static public String GAME_TITLE = "Drop7";
	final static public String MSG_FULL_STACK = "Game over!";
	final static public String MSG_WINNER = "The winner is Player: ";
	final static public String MSG_TIE = "GAME OVER!";
	final static public String MSG_NEXT = "Next pawn: ";
	final static public String MSG_DROP = "Next drop: ";
	
}
