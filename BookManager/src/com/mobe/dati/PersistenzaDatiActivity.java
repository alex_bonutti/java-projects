package com.mobe.dati;


import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PersistenzaDatiActivity extends Activity {
    private EditText editext;
    private DbManager dbManager;
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final ListView listView = (ListView) findViewById(R.id.bookList);
        dbManager = new DbManager(this);

        final ArrayList<String> arrayData = new ArrayList<String>();
    	ArrayList<Product> products = dbManager.getAllProducts();
    	for(Product p: products)
    		arrayData.add(p.getId() + ") " + p.getAuthor() + " - " + p.getName());

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.row, R.id.rowText, arrayData) {
			
			@Override
			// l'override serve perch� l'oggetto dell'adapter non � pi� standard e il riempimento del layout � un po' pi� raffinato
			public View getView(int position, View convertView, ViewGroup parent){	
				// Se non presente una istanza della View la creiamo attraverso
				// inflating. Se gi� presente la riutilizziamo (La cella gi� creata sarebbe quella uscita dalla schermata => caching dei dati)
				if(convertView==null){
					// Otteniamo il riferimento alla View da parserizzare
					LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = inflater.inflate(R.layout.row, null);					
				}

				// Otteniamo i campi da riempire
				TextView text = (TextView)convertView.findViewById(R.id.rowText);

				// Assegnamo i valori
				text.setText(arrayData.get(position));
				
				// Ritorniamo la View
				return convertView;
			}
		};
	
		//ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayData);
		// Lo impostiamo come adapter della listView
		listView.setAdapter(arrayAdapter);

		Button resButton = (Button) findViewById(R.id.add_book);
		resButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				// Andiamo in modo esplicito alla seconda act
				Intent intent2 = new Intent(PersistenzaDatiActivity.this,
						AddBook.class);
				startActivity(intent2);

			}

		});

    }
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Men� di gestione modalit� di visualizzazione che, per renderle checkable		
		int first = Menu.FIRST;
		MenuItem mapItem = menu.add(1, first, first, "Map");	
		mapItem.setIcon(android.R.drawable.ic_menu_mapmode);
		mapItem.setCheckable(true);
		mapItem.setChecked(true);		
		
		MenuItem satelliteItem = menu.add(1, first+1, first+1, "Satellite");
		satelliteItem.setIcon(android.R.drawable.ic_menu_compass);
		satelliteItem.setCheckable(true);
		satelliteItem.setChecked(false);
		
		// Visualizziamo il menu
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Modifichiamo lo stato di quella selezionata
		item.setChecked(!item.isChecked());
		// Abilitiamo o meno l'opzione relativa
		switch (item.getItemId()) {
			case Menu.FIRST:
				
				break;
			case Menu.FIRST + 1:
				
				break;
		}
		return super.onOptionsItemSelected(item);
	}
    
}