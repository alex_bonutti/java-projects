import java.awt.*;
import java.awt.event.*;
import java.text.*;

public class XmlPanel implements ActionListener {

	private Frame f;
	private TextField path_file, results;
	private Button b, c;

	private int elements = 2;
	private XmlHandler xmlHandler;

	public XmlPanel() {
		
		// default handler for SAX handler class  
		// all three methods are written in handler's body  
		xmlHandler = new XmlHandler(elements);
		
		// frame
		f = new Frame("XmlPanel");
		f.setSize(320, 150);
		f.setLocation(200, 200);
		
		// WindowAdapter e' gia' un'implementazione di WindowListener, che ha 6 metodi da implementare, e su questa implementiamo solo la funzione di chiusura che ci interessa
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we)
			{
				closePanel();
			}
		}
		);

		// textfield
		path_file = new TextField("student.xml");

		// button
		b = new Button("read Xml");
		// serve per aggiungere i metodi della classe corrente alle azioni del bottone 
		b.addActionListener(this);

		// raggruppa elementi grafici
		Panel p = new Panel();
		p.add(b);

		/* button
		c = new Button("close");
		c.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				closePanel();
			}
		}
		);
		Panel cc = new Panel();
		cc.add(c);*/

		results = new TextField("");

		// rendiamo visibili nel frame tutti gli elementi
		f.add(path_file, "North");
		f.add(p, "Center");
		f.add(results, "South");
		f.setVisible(true);
	}

	public void closePanel()
	{
		System.exit(1);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == c)
		{
			closePanel();
		}
		else
		{
			String path_to_xml = path_file.getText();
			ReadXML readXml = new ReadXML();  
			readXml.getXml(xmlHandler, path_to_xml);
			
			// dialog
			String names = "";
			for (int i=0; i < elements; i++)
				names += ", " + xmlHandler.getStudent(i).LastName();
			
			results.setText(names);
		}
	}
}
