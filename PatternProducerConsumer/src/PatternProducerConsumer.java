
public class PatternProducerConsumer {

	public static void main(String[] args) {
		Pallet pallet = new Pallet();
		Producer p = new Producer(pallet, "producer");
		Consumer c = new Consumer(pallet, "consumer");

		p.start();
		c.start();
	}

}
