
public class Producer extends Thread {
	
	private Pallet pallet;

	public Producer(Pallet p, String name) {
		pallet = p;
		setName(name);
	}

	public void run()
	{
		for (int i = 0; i < 10; i++)
		{
			pallet.put(i);
			System.out.println("producer " + getName() + " sent resource " + i + "...");
			try
			{
				sleep( (int)Math.random()*100 );
			}
			catch (InterruptedException ie) {}
		}
	}
}
