package com.mobe.dati;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DbManager extends SQLiteOpenHelper {
	private final static int DB_VERSION = 1;
	public final static String DB_NAME = "TEST_BOOKS";
	private SQLiteDatabase db;	
	
	
	public DbManager(Context context) 	{
		super(context, DB_NAME, null, DB_VERSION);
		this.db = this.getWritableDatabase();
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		String s = TableProducts.getQueryCreateTable();
		db.execSQL(s);

		s = TableProducts.insertInitialData();
		db.execSQL(s);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {		
	}

	
	
	public long addProduct(Product p) {
		ContentValues cv = TableProducts.toContentValues(p);
		long res = db.insert(TableProducts.TABLE_NAME, null, cv);
		return res;
	}
	
	
	public ArrayList<Product> getAllProducts() {
		ArrayList<Product> list = new ArrayList<Product>();
		Cursor c  = db.query(TableProducts.TABLE_NAME, TableProducts.COLUMNS, null, null, null, null, null);
		c.moveToPosition(0);
    	while(!c.isAfterLast()) {
    		Product product = TableProducts.fromCursor(c);
    		list.add(product);
    		c.moveToNext();
    	}
    	c.close();
    	return list;
	}
	
	
}
